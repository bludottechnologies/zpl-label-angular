import { Component, OnInit } from '@angular/core';
import { Label, Alignment, LabelConfig, SimpleTable } from 'zpl-label-generator';
// import { Label, Alignment, LabelConfig, SimpleTable } from '../../../../../_Bludot/_libraries/zpl-label-generator/src';
// import { testingChars } from '../../../../../_Bludot/_libraries/zpl-label-generator/src/globals';
import { LabelaryService } from './_services/labelary.service';
import { PreviewService } from './_services/preview.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  zpl: string = '';
  labelSrc: any;

  // var regularLetterPlain = "^FT(X),(Y)^A@I,28,29,TT0003M_^FH\\^CI17^F8^FD(LETTER)^FS^CI0";
  // var boldLetterPlain = "^FT(X),(Y)^A0I,28,28^FH\\^FD(LETTER)^FS";

  // var regularLetterMcauleys = "^FT(X),(Y)^A@N,18,22,TT0003M_^FH\\^CI17^F8^FD(LETTER)^FS^CI0";
  // var boldLetterMcauleys = "^FT(X),(Y)^A0N,18,22^FH\\^FD(LETTER)^FS";

  config: LabelConfig = {
    dpmm: 8,
    dpi: 203,
    widthMM: 60,
    heightMM: 100,
    defaultLineSpacing: 1,
    defaultPointSize: 6,
    // defaultFont: 'custom',
    defaultFont: '0',
    boldFont: '0',
    marginsMM: {
      top: 10,
      right: 5,
      bottom: 10,
      left: 5,
    },
  };

  label = new Label(this.config);

  constructor(private labelaryService: LabelaryService, private sanitizer: DomSanitizer, private previewService: PreviewService) {
    const sampleText = `Marty you gotta come back with me. Don't worry, I'll take care of the lightning, you take care of your pop. By the way, what happened today, did he ask her out? Over there, on my hope chest. I've never seen purple underwear before, Calvin. Don't pay any attention to him, he's in one of his moods. Sam, quit fiddling with that thing, come in here to dinner. Now let's see, you already know Lorraine, this is Milton, this is Sally, that's Toby, and over there in the playpen is little baby Joey. You're gonna be in the car with her.`;

    const quid = 'White Bread Baguette (50.00%) (**Cereals containing gluten**), Turkey Slices (45.00%), Beef Tomato (5.00%)';
    // this.label.addText('Back to the Future III', 20);
    // this.label.addText('1985', 10);
    // this.label.addSpacingMm(5);
    // this.label.addText('Synopsis', 10);
    // this.label.addSpacingMm(2);
    // this.label.addText(sampleText, 6);
    // this.label.addSpacingMm(2);
    // this.label.addFieldBlock(sampleText, 6, 20, Alignment.JUSTIFIED, '0', true, 50);
    // this.label.addSpacingMm(2);
    // this.label.addText(sampleText, 6);
    // this.label.addText('End of Label', 10);
    // this.zpl = this.label.generateZPL();

    const nutritionTable: SimpleTable = {
      config: {
        leftWidthPercent: 30,
        rightWidthPercent: 70,
      },
      rows: [
        {
          left: { value: `Nutrition`, pointSize: 6, lines: 2 },
          right: { value: `Typical Values per 100g`, pointSize: 6, lines: 2 },
        },
        {
          left: { value: `Energy`, pointSize: 6, lines: 1, font: '0' },
          right: { value: `1157 kj / 276.53 kcal`, pointSize: 6, lines: 1 },
        },
        {
          left: { value: `Fat`, pointSize: 6, lines: 1 },
          right: { value: `38.18g`, pointSize: 6, lines: 1 },
        },
        {
          left: { value: `Saturates`, pointSize: 6, lines: 1 },
          right: { value: `23.54g`, pointSize: 6, lines: 1 },
        },
        {
          left: { value: `Carbohydrates`, pointSize: 6, lines: 1 },
          right: { value: `20.51g`, pointSize: 6, lines: 1 },
        },
        {
          left: { value: `Sugars`, pointSize: 6, lines: 1 },
          right: { value: `20.35g`, pointSize: 6, lines: 1 },
        },
        {
          left: { value: `Protein`, pointSize: 6, lines: 1 },
          right: { value: `33.44g`, pointSize: 6, lines: 1 },
        },
        {
          left: { value: `Salt`, pointSize: 6, lines: 1 },
          right: { value: `22.47g`, pointSize: 6, lines: 1 },
        },
      ],
    };

    this.label.addSpacingMm(0);

    this.label.addText('BLT Sandwich', 16);

    this.label.addText('400g | 732 Calories', 8);
    this.label.addSpacingMm(2);

    this.label.addText('INGREDIENTS', 8);
    this.label.addText('For allergens, see ingredients in bold', 5);

    this.label.addSpacingMm(1);

    this.label.addText(quid, 6);

    this.label.addSpacingMm(5);

    this.label.addBarcode(1234567891234, 2, 20, 3);

    this.label.addSimpleTable(nutritionTable);

    this.label.addSpacingMm(4);

    this.label.addFieldBlock('£1.50', 24, 1, Alignment.CENTER, '0');

    this.label.addSpacingMm(4);

    this.label.addFieldBlock('', 24, 1, Alignment.CENTER, '0', false);
    this.label.addFieldBlock('AB', 24, 1, Alignment.LEFT, '0', false);
    this.label.addFieldBlock('CD', 24, 1, Alignment.RIGHT, '0', false);
    this.label.addFieldBlock('EF', 24, 1, Alignment.RIGHT, '0');

    this.label.addFieldBlock('Best Before: 04/02/2022', 6, 1, Alignment.CENTER);

    this.label.addSpacingMm(4);

    // let tempFontSize = 4.8;

    // this.label.addText('A For allergens, see ingredients in bold', tempFontSize, 'A');
    // this.label.addFieldBlock('A For allergens, see ingredients in bold', tempFontSize, 2, Alignment.LEFT, 'A');
    // this.label.addSpacingMm(2);

    // this.label.addText('P For allergens, see ingredients in bold', tempFontSize, 'P');
    // this.label.addFieldBlock('P For allergens, see ingredients in bold', tempFontSize, 2, Alignment.LEFT, 'P');
    // this.label.addSpacingMm(2);

    // this.testFontRatios(19);
    // this.testFontRatios(20);

    this.zpl = this.label.generateZPL();
  }

  // testFontRatios(fontSize: number) {
  //   testingChars.forEach((key) => {
  //     const string = `${fontSize} - ${key}${key}${key}${key}${key}${key}`;
  //     this.label.addText(string, fontSize, 'A');
  //     this.label.addFieldBlock(string, fontSize, 2, Alignment.LEFT, 'A');
  //   });
  // }

  ngOnInit() {
    const body = {
      dpmm: this.config.dpmm,
      width: this.config.widthMM,
      height: this.config.heightMM,
      index: 0,
      zpl: this.zpl,
    };

    // this.labelaryService.getLabel(body).subscribe((response) => {
    //   let objectURL = URL.createObjectURL(response.body);
    //   this.labelSrc = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    // });

    this.previewService.getLabel(body).subscribe((response) => {
      let objectURL = URL.createObjectURL(response.body);
      this.labelSrc = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    });
  }
}
