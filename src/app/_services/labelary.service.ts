import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LabelaryService {
  constructor(private http: HttpClient) {}

  getLabel(body: any): Observable<any> {
    const params = {
      dpmm: body.dpmm,
      width: body.width / 25.4, // convert to inches
      height: body.height / 25.4, // convert to inches
      index: body.index,
      zpl: body.zpl,
    };

    return this.http.post<any>(`http://api.labelary.com/v1/printers/${params.dpmm}dpmm/labels/${params.width}x${params.height}/${params.index}/`, params.zpl, this.buildHttpOptions()).pipe(
      map((result: HttpResponse<Blob>) => {
        return result;
      })
    );
  }

  buildHttpOptions(): {
    headers?:
      | HttpHeaders
      | {
          [header: string]: string | string[];
        };
  } {
    const httpOptions = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'image/png',
      }),
    };

    return httpOptions;
  }
}
